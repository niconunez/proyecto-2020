﻿using Microsoft.Web.Services3.Addressing;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Caching;
using System.Web.Mvc;
using WebApplication7.DAL;
using WebApplication7.Repositorio;

namespace WebApplication7.Controllers
{
    public class AcessoController : Controller
    {
        // GET: Acesso
        public ActionResult Login()
        {
            return View();
        }
        public UnidadGeneric _unidadG = new UnidadGeneric();

        [HttpPost]
        public ActionResult Login(string User, string Pass)
        {
            
            try
            {
                
                using (dbProyectoSP2Entities context = new dbProyectoSP2Entities())
                {

                    var sAdmin = "";
                    var oUsuario = (from d in context.Sp_Usuarios 
                                   where d.Email == User.Trim() && d.Contrasena == Pass.Trim() 
                                   select d).FirstOrDefault();
                    var rol = (from d in context.Sp_Usuarios
                              where d.Email == User.Trim() && d.Contrasena == Pass.Trim() && d.IdRol == 1
                              select d).FirstOrDefault();

                    var idUser = (from d in context.Sp_Usuarios
                                  where d.Email == User.Trim() && d.Contrasena == Pass.Trim()
                                  select d.IdUsuario).FirstOrDefault();

                    if (oUsuario == null)
                    {
                        ViewBag.Error = "Usuario o contraseña invalida";
                        return View();
                    }
                    Session["User"] = oUsuario;
                    Session["ValorCorreo"] = User;
                    Session["IdUser"] = idUser;

                    if(rol != null)
                    {
                        Session["Admin"] = sAdmin;
                    }
                    else
                    {
                        Session["Admin"] = null;
                    }
                }
                return RedirectToAction("Index", "Home");
            }
            catch (Exception ex)
            {
                ViewBag.Error = ex.Message;
                return View();
            }
            
        }

        public ActionResult Registrar()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Registrar(Sp_Usuarios spu)
        {
            dbProyectoSP2Entities context = new dbProyectoSP2Entities();
            var data = (from d in context.Sp_Usuarios
                        where d.Email == spu.Email
                        select d).FirstOrDefault();

            var data2 = (from d in context.Sp_Usuarios
                         where d.RutUsuario == spu.RutUsuario
                         select d).FirstOrDefault();


            if (data == null && data2 == null) {
                spu.IdRol = 2;
                _unidadG.GetRepositoryInstance<Sp_Usuarios>().Add(spu);
                ViewBag.Registrado = "Registrado correctamente. Inicie sesión";
                return View("Login");
            }
            else { 
            
                ViewBag.Error = "Email o rut ya existen";
                return View();
            }
            
        }
    }

}