﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebApplication7.DAL;
using WebApplication7.Filters;
using WebApplication7.Repositorio;

namespace WebApplication7.Controllers
{
    public class UserController : Controller
    {
        dbProyectoSP2Entities context = new dbProyectoSP2Entities();
        // GET: User
        [AuthorizeUser(idOperacion: 21)]
        public ActionResult Dashboard()
        {
            return View();
        }

        public UnidadGeneric _unidadG = new UnidadGeneric();

        [AuthorizeUser(idOperacion: 21)]
        public ActionResult Ventas()
        {
            int IdUsuario = (int)Session["IdUser"];

            ViewBag.IdUsuario = IdUsuario;
            List<Sp_Telefono> telefonosV = _unidadG.GetRepositoryInstance<Sp_Telefono>().GetAllRecordsIQueryable().Where(e => e.IdUsuario == IdUsuario).ToList();
            return View(telefonosV);
        }

        
        [AuthorizeUser(idOperacion: 21)]
        [Route("UpdateVenta/{IdTelefono:int}")]
        public ActionResult UpdateVenta(int IdTelefono)
        {
           

            return View(_unidadG.GetRepositoryInstance<Sp_Telefono>().GetFirstorDefault(IdTelefono));
        }

        [AuthorizeUser(idOperacion: 21)]
        [HttpPost]
        public ActionResult UpdateVenta(Sp_Telefono spv)
        {
           

            _unidadG.GetRepositoryInstance<Sp_Telefono>().Update(spv);

            return RedirectToAction("Ventas");
        }

        [AuthorizeUser(idOperacion: 21)]
        public ActionResult EliminarVenta(int IdTelefono)
        {
            return View(_unidadG.GetRepositoryInstance<Sp_Telefono>().GetFirstorDefault(IdTelefono));
        }


        [AuthorizeUser(idOperacion: 21)]
        [HttpPost]
        public ActionResult EliminarVenta(Sp_Telefono spv)
        {
            _unidadG.GetRepositoryInstance<Sp_Telefono>().Remove(spv);
            return RedirectToAction("Ventas");


        }

        [AuthorizeUser(idOperacion: 21)]
        public ActionResult Compras()
        {
            int IdUsuario = (int)Session["IdUser"];

            ViewBag.IdUsuario = IdUsuario;


            //var idV = context.Sp_Usuarios.FirstOrDefault(m => m.IdUsuario == );
            // var nombre = context.Sp_Usuarios.FirstOrDefault(m => m.IdUsuario == idVendedor).Nombre;

           // var apellido = context.Sp_Usuarios.FirstOrDefault(m => m.IdUsuario == idVendedor).Apellido;

           // ViewBag.Vendedor = nombre + " " + apellido;
            List<Sp_Boleta> telefonosC = _unidadG.GetRepositoryInstance<Sp_Boleta>().GetAllRecordsIQueryable().Where(e => e.IdComprador == IdUsuario).ToList();
            

            return View(telefonosC);
        }

        public ActionResult CerrarSesion()
        {
            Session["User"] = null;
            Session["Admin"] = null;
            Session["IdUser"] = null;
            return RedirectToAction("Index","Home");
        }
    }
}