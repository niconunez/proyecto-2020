﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Web;
using System.Web.Mvc;
using WebApplication7.DAL;
using WebApplication7.Filters;
using WebApplication7.Repositorio;

namespace WebApplication7.Controllers
{
    public class AdminController : Controller
    {
        // GET: Admin
        [AuthorizeUser(idOperacion:1)]
        public ActionResult Dashboard()
        {
            return View();
        }

        public UnidadGeneric _unidadG = new UnidadGeneric();

        [AuthorizeUser(idOperacion: 2)]
        public ActionResult Publicaciones()
        {
            List<Sp_Telefono> telefonos = _unidadG.GetRepositoryInstance<Sp_Telefono>().GetAllRecordsIQueryable().Where(e => e.EsActivo == false || e.EsActivo == true || e.EsActivo == null).ToList();
            return View(telefonos);
        }

        [AuthorizeUser(idOperacion: 2)]
        public ActionResult Usuarios()
        {
            List<Sp_Usuarios> usuarios = _unidadG.GetRepositoryInstance<Sp_Usuarios>().GetAllRecordsIQueryable().Where(e => e.EsActivo == false || e.EsActivo == true || e.EsActivo == null).ToList();
            return View(usuarios);
        }

        [AuthorizeUser(idOperacion: 4)]
        public ActionResult UpdateUser(int IdUsuario)
        {
            return View(_unidadG.GetRepositoryInstance<Sp_Usuarios>().GetFirstorDefault(IdUsuario));
        }


        [AuthorizeUser(idOperacion: 5)]
        [HttpPost]
        public ActionResult UpdateUser(Sp_Usuarios spu)
        {
            _unidadG.GetRepositoryInstance<Sp_Usuarios>().Update(spu);

            return RedirectToAction("Usuarios");
        }

        [AuthorizeUser(idOperacion: 3)]
        public ActionResult AprobarPubli()
        {
            return UpdatePubli(0);
        }

        //Editar publicacion
        [AuthorizeUser(idOperacion: 4)]
        public ActionResult UpdatePubli(int IdTelefono)
        {
            /*
                        TelefonoDetalles td;

                        if(IdTelefono !== null)
                        {
                            td = JsonConvert.DeserializeObject<TelefonoDetalles>(JsonConvert.SerializeObject(_unidadG.GetRepositoryInstance<Sp_Telefono>().GetFirstorDefault(IdTelefono)));
                        }
                        else
                        {
                            td = new TelefonoDetalles();
                        }
                        return View("UpdatePubli",td); */

            return View(_unidadG.GetRepositoryInstance<Sp_Telefono>().GetFirstorDefault(IdTelefono));
        }

        [AuthorizeUser(idOperacion: 5)]
        [HttpPost]
        public ActionResult UpdatePubli(Sp_Telefono sp)
        {
            /*
                        TelefonoDetalles td;

                        if(IdTelefono !== null)
                        {
                            td = JsonConvert.DeserializeObject<TelefonoDetalles>(JsonConvert.SerializeObject(_unidadG.GetRepositoryInstance<Sp_Telefono>().GetFirstorDefault(IdTelefono)));
                        }
                        else
                        {
                            td = new TelefonoDetalles();
                        }
                        return View("UpdatePubli",td); */

            _unidadG.GetRepositoryInstance<Sp_Telefono>().Update(sp);

            return RedirectToAction("Publicaciones");
        }

        //Eliminar publicacion

        [AuthorizeUser(idOperacion: 6)]
        public ActionResult EliminarPubli(int IdTelefono)
        {
            return View(_unidadG.GetRepositoryInstance<Sp_Telefono>().GetFirstorDefault(IdTelefono));
        }


        [AuthorizeUser(idOperacion: 7)]
        [HttpPost]
        public ActionResult EliminarPubli(Sp_Telefono sp)
        {
            _unidadG.GetRepositoryInstance<Sp_Telefono>().Remove(sp);
            return RedirectToAction("Publicaciones");


        }


        [AuthorizeUser(idOperacion: 8)]
        public ActionResult ServicioCliente()
        {
            List<Sp_Ticket> tickets = _unidadG.GetRepositoryInstance<Sp_Ticket>().GetAllRecordsIQueryable().Where(e => e.EsActivo == false || e.EsActivo == true || e.EsActivo == null).ToList();
            return View(tickets);
        }

        //
        [AuthorizeUser(idOperacion: 9)]
        public ActionResult UpdateTicket(int IdTicket)
        {

            return View(_unidadG.GetRepositoryInstance<Sp_Ticket>().GetFirstorDefault(IdTicket));
        }



        [AuthorizeUser(idOperacion: 10)]
        [HttpPost]
        public ActionResult UpdateTicket(Sp_Ticket sptick)
        {
            _unidadG.GetRepositoryInstance<Sp_Ticket>().Update(sptick);

            string EmailOrigen = "sellphonesoporte@gmail.com";
            string EmailDestino = sptick.Email;
            string Rpta = sptick.Respuesta;
            string Contraseña = "sellphone12";

            MailMessage mailMessage = new MailMessage(EmailOrigen, EmailDestino, "Respuesta ticket N°" + sptick.IdTicket, Rpta);

            mailMessage.IsBodyHtml = true;
            SmtpClient smtpClient = new SmtpClient("smtp.gmail.com");
            smtpClient.EnableSsl = true;
            smtpClient.UseDefaultCredentials = false;
            smtpClient.Host = "smtp.gmail.com";

            smtpClient.Port = 587;
            smtpClient.Credentials = new System.Net.NetworkCredential(EmailOrigen, Contraseña);
            smtpClient.Send(mailMessage);

            smtpClient.Dispose();


            return RedirectToAction("ServicioCliente");
        }      
    }
}