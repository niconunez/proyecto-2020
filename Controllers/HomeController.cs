﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Transbank.Webpay;
using WebApplication7.DAL;
using WebApplication7.Filters;
using WebApplication7.Models;
using WebApplication7.Models.Home;
using WebApplication7.Repositorio;

namespace WebApplication7.Controllers
{
    public class HomeController : Controller
    {
        dbProyectoSP2Entities context = new dbProyectoSP2Entities();

        public UnidadGeneric _unidadG = new UnidadGeneric();

        
        [AllowAnonymous]
        public ActionResult Index()
        {
            return View();
        }




        /* [HttpPost]
         public ActionResult Login(SpUsuarios model, string returnUrl)
         {

             Entities context = new Entities();
             var data = context.SpUsuarios.Where(x => x.Email == model.Email && x.Contrasena == model.Contrasena).First();
             if (data != null)
             {
                 FormsAuthentication.SetAuthCookie(data.Nombre, false);
                 if (Url.IsLocalUrl(returnUrl) && returnUrl.Length > 1 && returnUrl.StartsWith("/")
                     && !returnUrl.StartsWith("//") && !returnUrl.StartsWith("/\\"))
                 {
                     return Redirect(returnUrl);
                 }
                 else
                 {
                     return RedirectToAction("Index");
                 }
             }
             else
             {
                 ModelState.AddModelError("", "Rut/Contraseña invalida");
                 return View();
             }


         }

         [Authorize]
         public ActionResult CerrarSesion()
         {
             FormsAuthentication.SignOut();


             return RedirectToAction("Login", "Home");
         }
         */


        [AuthorizeUser(idOperacion: 21)]
        public ActionResult AgregarPubli()
        {
            return View();
        }

         [AuthorizeUser(idOperacion: 22)]
        [HttpPost]
        public ActionResult AgregarPubli(Sp_Telefono spt, HttpPostedFileBase file)
        {
            string pic = null;
            if (file != null)
            {
                pic = System.IO.Path.GetFileName(file.FileName);
                string path = System.IO.Path.Combine(Server.MapPath("/TelefonoImg/"), pic);
                //archivo subido
                file.SaveAs(path);

            }
            spt.Foto = pic;
            spt.IdUsuario = (int)Session["IdUser"];
            _unidadG.GetRepositoryInstance<Sp_Telefono>().Add(spt);
            return RedirectToAction("Index");
        }




        public ActionResult AddCarrito(int IdTelefono)
        {
            if (Session["carro"] == null)
            {
                List<Item> carro = new List<Item>();
                var telefono = context.Sp_Telefono.Find(IdTelefono);
                carro.Add(new Item()
                {
                    Telefono = telefono,
                    Cantidad = 1
                });

                Session["carro"] = carro;

            }
            else
            {
                List<Item> carro = (List<Item>)Session["carro"];
                var telefono = context.Sp_Telefono.Find(IdTelefono);
                foreach (var item in carro)
                {
                    if (item.Telefono.IdTelefono == IdTelefono)
                    {
                        int cantidadA = item.Cantidad;
                        carro.Remove(item);
                        carro.Add(new Item()
                        {
                            Telefono = telefono,
                            Cantidad = cantidadA + 1
                        });


                        break;
                    }

                    else
                    {
                        carro.Add(new Item()
                        {
                            Telefono = telefono,
                            Cantidad = 1
                        });

                    }
                }

                Session["carro"] = carro;
            }
            return Redirect("Checkout");
        }




        public ActionResult RemoveCarrito(int IdTelefono)
        {
            List<Item> carro = (List<Item>)Session["carro"];
            // var telefono = context.Sp_Telefono.Find(IdTelefono);
            foreach (var item in carro)
            {
                if (item.Telefono.IdTelefono == IdTelefono)
                {
                    carro.Remove(item);
                    break;
                }
            }
            Session["carro"] = null;
            return Redirect("Comprar");

        }



        public ActionResult Comprar()
        {

            HomeIndexViewModel model = new HomeIndexViewModel();
            return View(model.CreaModel());
        }


        [AuthorizeUser(idOperacion: 23)]
        public ActionResult Checkout(Sp_Boleta spb)
        {

            int precio = 1;
            int idTelefono = 0;
            int total = 0;
            if (Session["carro"] != null)
            {
                foreach (Item item in (List<Item>)Session["carro"])
                {

                    precio = (int)item.Telefono.Precio;
                    idTelefono = (int)item.Telefono.IdTelefono;
                    total = (int)precio;
                    //ViewBag.IdVendedor = (int)item.Telefono.IdUsuario;
                }
            }
            else
            {
                return View();
            }

            /* if (telefono.IdTelefono == IdTelefono){

                 amount = telefono.Precio;
             }

             */





            var transaction = new Webpay(Configuration.ForTestingWebpayPlusNormal()).NormalTransaction;
                var amount = precio;
            var buyOrder = new Random().Next(100000, 999999999).ToString();
            var sessionId = "sessionId";

            string returnUrl = "http://localhost:44390/home/Return";
            string finalUrl = "http://localhost:44390/home/Final";

            AppContext.SetSwitch("Switch.System.Security.Cryptography.Xml.UseInsecureHashAlgorithms", true);
            AppContext.SetSwitch("Switch.System.Security.Cryptography.Pkcs.UseInsecureHashAlgorithms", true);
            var initResult = transaction.initTransaction(amount, buyOrder, sessionId, returnUrl, finalUrl);

            var tokenWs = initResult.token;
            var formAction = initResult.url;
            

            
            ViewBag.Amount = amount;
            ViewBag.BuyOrder = buyOrder;
            ViewBag.TokenWs = tokenWs;
            ViewBag.FormAction = formAction;


            spb.IdTelefono = idTelefono;
            spb.Total = total;
            spb.IdComprador = (int?)Session["IdUser"];

            

            var dataExiste = context.Sp_Telefono.Where(x => x.IdTelefono == spb.IdTelefono).First();
            int anadido = 0;
            if (dataExiste != null){

                
                _unidadG.GetRepositoryInstance<Sp_Telefono>().UpdateByWhereClause(x => x.IdTelefono == spb.IdTelefono, x => x.Vendido = true);
                _unidadG.GetRepositoryInstance<Sp_Boleta>().Add(spb);
                anadido = 1;

                /*  if(anadido == 1)
                  {
                      Session["carro"] = null;
                      return View();
                  }



              }
              else
              {
                  Session["carro"] = null;
                  return View();
*/
            }


            return View();




            
        }

        public ActionResult Return()
        {

            AppContext.SetSwitch("Switch.System.Security.Cryptography.Xml.UseInsecureHashAlgorithms", true);
            AppContext.SetSwitch("Switch.System.Security.Cryptography.Pkcs.UseInsecureHashAlgorithms", true);
            var transaction = new Webpay(Configuration.ForTestingWebpayPlusNormal()).NormalTransaction;
            string tokenWs = Request.Form["token_ws"];

            var result = transaction.getTransactionResult(tokenWs);
            var output = result.detailOutput[0];
            if (output.responseCode == 0)
            {
                ViewBag.UrlRedirection = result.urlRedirection;
                ViewBag.TokenWs = tokenWs;
                ViewBag.ResponseCode = output.responseCode;
                ViewBag.Amount = output.amount;
                ViewBag.AuthorizationCode = output.authorizationCode;
                

            }
            
            return View();

        }


        public ActionResult Final()
        {
            AppContext.SetSwitch("Switch.System.Security.Cryptography.Xml.UseInsecureHashAlgorithms", true);
            AppContext.SetSwitch("Switch.System.Security.Cryptography.Pkcs.UseInsecureHashAlgorithms", true);

            // _unidadG.GetRepositoryInstance<Sp_Boleta>().Add
            return View();
        }

        
        public ActionResult AgregarTicket()
        {
            return RedirectToAction("Index");
        }

        
        [HttpPost]
        public ActionResult AgregarTicket(Sp_Ticket spti)
        {

            _unidadG.GetRepositoryInstance<Sp_Ticket>().Add(spti);
            return RedirectToAction("Index");
        }





        /* public ActionResult DetallesPubli(int IdTelefono)
        {
            Sp_Telefono sptl = _unidadG.GetRepositoryInstance<Sp_Telefono>().GetFirstorDefault(IdTelefono);
            _unidadG.GetRepositoryInstance<Sp_Telefono>().GetFirstorDefault(IdTelefono);
            /* if (sptl == null)
                return View("Index");
            else 
            return View("DetallesPubli", IdTelefono);
            
        } 
    */
        
        public ActionResult DetallesPubli(int? idtelefono)
        {
            if (idtelefono == null)
            {
                return View("Index");
            }

            var publicacion = context.Sp_Telefono.FirstOrDefault(m => m.IdTelefono == idtelefono);
            if (publicacion == null)
            {
                return View("Index");
            }

            return View(publicacion);
        }





        public ActionResult CerrarSesion()
        {
            Session["User"] = null;
            Session["Admin"] = null;
            Session["IdUser"] = null;
            return View("Index");
        }

        
    }
}