﻿using System.Collections.Generic;
using System.Linq;
using System.Web;
using WebApplication7.DAL;

namespace WebApplication7.Models
{
    public class Item
    {
        public Sp_Telefono Telefono { get; set; }
        public int Cantidad { get; set; }
    }
}