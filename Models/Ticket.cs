﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApplication7.Models
{
    public class Ticket
    {
        public int IdTicket { get; set; }

        public string Descripcion { get; set; }
        public Nullable<bool> EsActivo { get; set; }
        public string Email { get; set; }
    }
}