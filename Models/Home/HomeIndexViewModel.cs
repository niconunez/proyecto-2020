﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WebApplication7.DAL;
using WebApplication7.Repositorio;

namespace WebApplication7.Models.Home
{
    public class HomeIndexViewModel
    {
        public UnidadGeneric _unidadG = new UnidadGeneric();

        public IEnumerable<Sp_Telefono> ListaPublis { get; set; }

        public HomeIndexViewModel CreaModel()
        {
            return new HomeIndexViewModel()
            {
                ListaPublis = _unidadG.GetRepositoryInstance<Sp_Telefono>().GetAllRecords().Where(e => e.EsActivo == true && e.Vendido != true)

            };
        }
    }
}