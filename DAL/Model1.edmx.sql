
-- --------------------------------------------------
-- Entity Designer DDL Script for SQL Server 2005, 2008, 2012 and Azure
-- --------------------------------------------------
-- Date Created: 06/29/2020 18:37:30
-- Generated from EDMX file: C:\Users\nicon\source\repos\WebApplication7\DAL\Model1.edmx
-- --------------------------------------------------

SET QUOTED_IDENTIFIER OFF;
GO
USE [dbProyectoSP2];
GO
IF SCHEMA_ID(N'dbo') IS NULL EXECUTE(N'CREATE SCHEMA [dbo]');
GO

-- --------------------------------------------------
-- Dropping existing FOREIGN KEY constraints
-- --------------------------------------------------

IF OBJECT_ID(N'[dbo].[FK__Sp_Carrit__IdTel__2E1BDC42]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Sp_Carrito] DROP CONSTRAINT [FK__Sp_Carrit__IdTel__2E1BDC42];
GO
IF OBJECT_ID(N'[dbo].[FK__Sp_RolUsu__IdRol__49C3F6B7]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Sp_RolUsuario] DROP CONSTRAINT [FK__Sp_RolUsu__IdRol__49C3F6B7];
GO
IF OBJECT_ID(N'[dbo].[FK__Sp_Usuari__IdRol__71D1E811]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Sp_Usuarios] DROP CONSTRAINT [FK__Sp_Usuari__IdRol__71D1E811];
GO

-- --------------------------------------------------
-- Dropping existing tables
-- --------------------------------------------------

IF OBJECT_ID(N'[dbo].[Sp_Boleta]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Sp_Boleta];
GO
IF OBJECT_ID(N'[dbo].[Sp_Carrito]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Sp_Carrito];
GO
IF OBJECT_ID(N'[dbo].[Sp_EnvioDetalles]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Sp_EnvioDetalles];
GO
IF OBJECT_ID(N'[dbo].[Sp_EstadoCarrito]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Sp_EstadoCarrito];
GO
IF OBJECT_ID(N'[dbo].[Sp_Roles]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Sp_Roles];
GO
IF OBJECT_ID(N'[dbo].[Sp_RolUsuario]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Sp_RolUsuario];
GO
IF OBJECT_ID(N'[dbo].[Sp_Telefono]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Sp_Telefono];
GO
IF OBJECT_ID(N'[dbo].[Sp_Ticket]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Sp_Ticket];
GO
IF OBJECT_ID(N'[dbo].[Sp_Usuarios]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Sp_Usuarios];
GO

-- --------------------------------------------------
-- Creating all tables
-- --------------------------------------------------

-- Creating table 'Sp_Boleta'
CREATE TABLE [dbo].[Sp_Boleta] (
    [IdBoleta] int IDENTITY(1,1) NOT NULL,
    [IdTelefono] int  NULL,
    [Total] int  NULL
);
GO

-- Creating table 'Sp_Carrito'
CREATE TABLE [dbo].[Sp_Carrito] (
    [IdCarrito] int IDENTITY(1,1) NOT NULL,
    [IdTelefono] int  NULL,
    [RutUsuario] int  NULL,
    [IdEstadoCarrito] int  NULL
);
GO

-- Creating table 'Sp_EnvioDetalles'
CREATE TABLE [dbo].[Sp_EnvioDetalles] (
    [EnvioDetalleId] int IDENTITY(1,1) NOT NULL,
    [RutUsuario] int  NULL,
    [Direccion] varchar(100)  NULL,
    [Ciudad] varchar(50)  NULL,
    [Comuna] varchar(50)  NULL,
    [IdOrden] int  NULL,
    [MontoPagado] decimal(18,0)  NULL,
    [MetodoPago] varchar(100)  NULL
);
GO

-- Creating table 'Sp_EstadoCarrito'
CREATE TABLE [dbo].[Sp_EstadoCarrito] (
    [IdEstadoCarrito] int IDENTITY(1,1) NOT NULL,
    [EstadoCarrito] varchar(50)  NULL
);
GO

-- Creating table 'Sp_Roles'
CREATE TABLE [dbo].[Sp_Roles] (
    [IdRol] int IDENTITY(1,1) NOT NULL,
    [NombreRol] varchar(100)  NULL
);
GO

-- Creating table 'Sp_RolUsuario'
CREATE TABLE [dbo].[Sp_RolUsuario] (
    [IdRolUsuario] int IDENTITY(1,1) NOT NULL,
    [IdRol] int  NULL,
    [IdOperacion] int  NULL
);
GO

-- Creating table 'Sp_Telefono'
CREATE TABLE [dbo].[Sp_Telefono] (
    [IdTelefono] int IDENTITY(1,1) NOT NULL,
    [Marca] varchar(50)  NULL,
    [Modelo] varchar(50)  NULL,
    [Estado] varchar(50)  NULL,
    [Especs] varchar(50)  NULL,
    [Precio] int  NULL,
    [Foto] varchar(200)  NULL,
    [EsActivo] bit  NULL,
    [EsInactivo] bit  NULL
);
GO

-- Creating table 'Sp_Ticket'
CREATE TABLE [dbo].[Sp_Ticket] (
    [IdTicket] int IDENTITY(1,1) NOT NULL,
    [Email] varchar(200)  NULL,
    [Descripcion] varchar(200)  NULL,
    [Respuesta] varchar(200)  NULL,
    [EsActivo] bit  NULL
);
GO

-- Creating table 'Sp_Usuarios'
CREATE TABLE [dbo].[Sp_Usuarios] (
    [IdUsuario] int IDENTITY(1,1) NOT NULL,
    [RutUsuario] varchar(12)  NULL,
    [Nombre] varchar(50)  NULL,
    [Apellido] varchar(50)  NULL,
    [Direccion] varchar(50)  NULL,
    [Ciudad] varchar(50)  NULL,
    [Email] varchar(100)  NULL,
    [Contrasena] varchar(100)  NULL,
    [IdRol] int  NULL,
    [EsActivo] bit  NULL,
    [EsInactivo] bit  NULL
);
GO

-- --------------------------------------------------
-- Creating all PRIMARY KEY constraints
-- --------------------------------------------------

-- Creating primary key on [IdBoleta] in table 'Sp_Boleta'
ALTER TABLE [dbo].[Sp_Boleta]
ADD CONSTRAINT [PK_Sp_Boleta]
    PRIMARY KEY CLUSTERED ([IdBoleta] ASC);
GO

-- Creating primary key on [IdCarrito] in table 'Sp_Carrito'
ALTER TABLE [dbo].[Sp_Carrito]
ADD CONSTRAINT [PK_Sp_Carrito]
    PRIMARY KEY CLUSTERED ([IdCarrito] ASC);
GO

-- Creating primary key on [EnvioDetalleId] in table 'Sp_EnvioDetalles'
ALTER TABLE [dbo].[Sp_EnvioDetalles]
ADD CONSTRAINT [PK_Sp_EnvioDetalles]
    PRIMARY KEY CLUSTERED ([EnvioDetalleId] ASC);
GO

-- Creating primary key on [IdEstadoCarrito] in table 'Sp_EstadoCarrito'
ALTER TABLE [dbo].[Sp_EstadoCarrito]
ADD CONSTRAINT [PK_Sp_EstadoCarrito]
    PRIMARY KEY CLUSTERED ([IdEstadoCarrito] ASC);
GO

-- Creating primary key on [IdRol] in table 'Sp_Roles'
ALTER TABLE [dbo].[Sp_Roles]
ADD CONSTRAINT [PK_Sp_Roles]
    PRIMARY KEY CLUSTERED ([IdRol] ASC);
GO

-- Creating primary key on [IdRolUsuario] in table 'Sp_RolUsuario'
ALTER TABLE [dbo].[Sp_RolUsuario]
ADD CONSTRAINT [PK_Sp_RolUsuario]
    PRIMARY KEY CLUSTERED ([IdRolUsuario] ASC);
GO

-- Creating primary key on [IdTelefono] in table 'Sp_Telefono'
ALTER TABLE [dbo].[Sp_Telefono]
ADD CONSTRAINT [PK_Sp_Telefono]
    PRIMARY KEY CLUSTERED ([IdTelefono] ASC);
GO

-- Creating primary key on [IdTicket] in table 'Sp_Ticket'
ALTER TABLE [dbo].[Sp_Ticket]
ADD CONSTRAINT [PK_Sp_Ticket]
    PRIMARY KEY CLUSTERED ([IdTicket] ASC);
GO

-- Creating primary key on [IdUsuario] in table 'Sp_Usuarios'
ALTER TABLE [dbo].[Sp_Usuarios]
ADD CONSTRAINT [PK_Sp_Usuarios]
    PRIMARY KEY CLUSTERED ([IdUsuario] ASC);
GO

-- --------------------------------------------------
-- Creating all FOREIGN KEY constraints
-- --------------------------------------------------

-- Creating foreign key on [IdTelefono] in table 'Sp_Carrito'
ALTER TABLE [dbo].[Sp_Carrito]
ADD CONSTRAINT [FK__Sp_Carrit__IdTel__2E1BDC42]
    FOREIGN KEY ([IdTelefono])
    REFERENCES [dbo].[Sp_Telefono]
        ([IdTelefono])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK__Sp_Carrit__IdTel__2E1BDC42'
CREATE INDEX [IX_FK__Sp_Carrit__IdTel__2E1BDC42]
ON [dbo].[Sp_Carrito]
    ([IdTelefono]);
GO

-- Creating foreign key on [IdRol] in table 'Sp_Usuarios'
ALTER TABLE [dbo].[Sp_Usuarios]
ADD CONSTRAINT [FK__Sp_Usuari__IdRol__276EDEB3]
    FOREIGN KEY ([IdRol])
    REFERENCES [dbo].[Sp_Roles]
        ([IdRol])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK__Sp_Usuari__IdRol__276EDEB3'
CREATE INDEX [IX_FK__Sp_Usuari__IdRol__276EDEB3]
ON [dbo].[Sp_Usuarios]
    ([IdRol]);
GO

-- Creating foreign key on [IdRol] in table 'Sp_RolUsuario'
ALTER TABLE [dbo].[Sp_RolUsuario]
ADD CONSTRAINT [FK__Sp_RolUsu__IdRol__49C3F6B7]
    FOREIGN KEY ([IdRol])
    REFERENCES [dbo].[Sp_Roles]
        ([IdRol])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK__Sp_RolUsu__IdRol__49C3F6B7'
CREATE INDEX [IX_FK__Sp_RolUsu__IdRol__49C3F6B7]
ON [dbo].[Sp_RolUsuario]
    ([IdRol]);
GO

-- --------------------------------------------------
-- Script has ended
-- --------------------------------------------------