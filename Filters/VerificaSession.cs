﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebApplication7.Controllers;
using WebApplication7.DAL;

namespace WebApplication7.Filters
{
    public class VerificaSession : ActionFilterAttribute
    {
        private Sp_Usuarios usuario;
        public override void OnActionExecuted(ActionExecutedContext filterContext)
        {
            try
            {
                base.OnActionExecuted(filterContext);
                usuario = (Sp_Usuarios)HttpContext.Current.Session["User"];
               /* if(usuario == null)                 
                {
                    if(filterContext.Controller is AcessoController == false)
                    {
                        filterContext.HttpContext.Response.Redirect("/Acesso/Login");
                    }

                    
                } */
            }
            catch (Exception)
            {
                filterContext.Result = new RedirectResult("~/Acesso/Login");
            }
        }
    }
}