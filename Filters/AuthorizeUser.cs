﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebApplication7.DAL;

namespace WebApplication7.Filters
{

    [AttributeUsage(AttributeTargets.Method, AllowMultiple = false)]
    public class AuthorizeUser  : AuthorizeAttribute
    {
        private Sp_Usuarios usuario;
        private dbProyectoSP2Entities context = new dbProyectoSP2Entities();
        private int idOperacion;
            
        public AuthorizeUser(int idOperacion = 0)
        {
            this.idOperacion = idOperacion;
        }

        public override void OnAuthorization(AuthorizationContext filterContext)
        {
            String nombreRol = "";
            try
            {
                usuario = (Sp_Usuarios)HttpContext.Current.Session["User"];
                var lstMisOperaciones = from m in context.Sp_RolUsuario
                                        where m.IdRol == usuario.IdRol
                                        && m.IdOperacion == idOperacion
                                        select m;

                if(lstMisOperaciones.ToList().Count() < 1) {

                    filterContext.Result = new RedirectResult("~/Error/Error");

                }
            }
            catch(Exception ex)
            {
                filterContext.Result = new RedirectResult("~/Error/Error");
            }
            // base.OnAuthorization(filterContext);
        }
    }
}   
