﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Web;

namespace WebApplication7.Repositorio
{
    public interface IRepository<Sp_Entity> where Sp_Entity : class
    {
        IEnumerable<Sp_Entity> GetAllRecords();
        IEnumerable<Sp_Entity> GetTelefono();
        // Sp_Entity GetTelefonoById(int IdTelefono);
        IQueryable<Sp_Entity> GetAllRecordsIQueryable();
        int GetAllrecordCount();
        void Add(Sp_Entity entity);
        void Update(Sp_Entity entity);
        void UpdateByWhereClause(Expression<Func<Sp_Entity, bool>> wherePredict, Action<Sp_Entity> ForEachPredict);
        Sp_Entity GetFirstorDefault(int recordId);
        void Remove(Sp_Entity entity);
        void RemoveByWhereClause(Expression<Func<Sp_Entity, bool>> wherePredict);
        void RemoveRangeByWhereClause(Expression<Func<Sp_Entity, bool>> wherePredict);
        void InactiveAndDeleteMarkByWhereClause(Expression<Func<Sp_Entity, bool>> wherePredict, Action<Sp_Entity> ForEachPredict);
        Sp_Entity GetFirstorDefaultByParameter(Expression<Func<Sp_Entity, bool>> wherePredict);

        IEnumerable<Sp_Entity> GetListParameter(Expression<Func<Sp_Entity, bool>> wherePredict);

        IEnumerable<Sp_Entity> GetResultBySqlProcedure(string query, params object[] parameters);
        IEnumerable<Sp_Entity> GetRecordsToShow(int PageNo, int PageSize, int CurrentPage, Expression<Func<Sp_Entity, bool>> wherePredict, Expression<Func<Sp_Entity, int>> orderByPredict);
    }
}