﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WebApplication7.DAL;

namespace WebApplication7.Repositorio
{
    public class UnidadGeneric : IDisposable
    {
        private dbProyectoSP2Entities DBEntity = new dbProyectoSP2Entities();

        public IRepository<Sp_EntityType> GetRepositoryInstance<Sp_EntityType>() where Sp_EntityType : class
        {
            return new RepositorioGeneric<Sp_EntityType>(DBEntity);
        }

        public void SaveChanges()
        {
            DBEntity.SaveChanges();
        }

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    DBEntity.Dispose();
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {

            Dispose(true);
            GC.SuppressFinalize(this);

        }

        private bool disposed = false;
    }
}