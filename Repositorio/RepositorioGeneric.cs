﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using WebApplication7.DAL;

namespace WebApplication7.Repositorio
{
    public class RepositorioGeneric<Sp_Entity> : IRepository<Sp_Entity> where Sp_Entity : class
    {

        DbSet<Sp_Entity> _dbSet;

        private dbProyectoSP2Entities _DBEntity;

        public RepositorioGeneric(dbProyectoSP2Entities DbEntity)
        {
            _DBEntity = DbEntity;
            _dbSet = _DBEntity.Set<Sp_Entity>();

        }
        public void Add(Sp_Entity entity)
        {
            _dbSet.Add(entity);

            _DBEntity.SaveChanges();
        }

        public int GetAllrecordCount()
        {
            return _dbSet.Count();
        }

        public IEnumerable<Sp_Entity> GetAllRecords()
        {
            return _dbSet.ToList();
        }

        public IQueryable<Sp_Entity> GetAllRecordsIQueryable()
        {
            return _dbSet;
        }

        public Sp_Entity GetFirstorDefault(int recordId)
        {
            return _dbSet.Find(recordId);
        }

        public Sp_Entity GetFirstorDefaultByParameter(Expression<Func<Sp_Entity, bool>> wherePredict)
        {
            return _dbSet.Where(wherePredict).FirstOrDefault();
        }

        public IEnumerable<Sp_Entity> GetListParameter(Expression<Func<Sp_Entity, bool>> wherePredict)
        {
            return _dbSet.Where(wherePredict).ToList();
        }

        public IEnumerable<Sp_Entity> GetRecordsToShow(int PageNo, int PageSize, int CurrentPage, Expression<Func<Sp_Entity, bool>> wherePredict, Expression<Func<Sp_Entity, int>> orderByPredict)
        {
            if (wherePredict != null)
            {
                return _dbSet.OrderBy(orderByPredict).Where(wherePredict).ToList();

            }
            else
            {
                return _dbSet.OrderBy(orderByPredict).ToList();
            }
        }

        public IEnumerable<Sp_Entity> GetResultBySqlProcedure(string query, params object[] parameters)
        {
            if (parameters != null)
            {
                return _DBEntity.Database.SqlQuery<Sp_Entity>(query, parameters).ToList();
            }
            else
            {
                return _DBEntity.Database.SqlQuery<Sp_Entity>(query).ToList();
            }
        }

        public IEnumerable<Sp_Entity> GetTelefono()
        {
            throw new NotImplementedException();
        }

        public void InactiveAndDeleteMarkByWhereClause(Expression<Func<Sp_Entity, bool>> wherePredict, Action<Sp_Entity> ForEachPredict)
        {
            _dbSet.Where(wherePredict).ToList().ForEach(ForEachPredict);
        }

        public void Remove(Sp_Entity entity)
        {
            if (_DBEntity.Entry(entity).State == EntityState.Detached) { }
            _dbSet.Attach(entity);
            _dbSet.Remove(entity);
            _DBEntity.SaveChanges();
        }



        public void RemoveByWhereClause(Expression<Func<Sp_Entity, bool>> wherePredict)
        {
            Sp_Entity entity = _dbSet.Where(wherePredict).FirstOrDefault();
            Remove(entity);
        }

        public void RemoveRangeByWhereClause(Expression<Func<Sp_Entity, bool>> wherePredict)
        {
            List<Sp_Entity> entity = _dbSet.Where(wherePredict).ToList();
            foreach (var e in entity)
            {
                Remove(e);
            }
        }

        public void Update(Sp_Entity entity)
        {
            _dbSet.Attach(entity);
            _DBEntity.Entry(entity).State = EntityState.Modified;
            _DBEntity.SaveChanges();
        }

        public void UpdateByWhereClause(Expression<Func<Sp_Entity, bool>> wherePredict, Action<Sp_Entity> ForEachPredict)
        {

            _dbSet.Where(wherePredict).ToList().ForEach(ForEachPredict);
            _DBEntity.SaveChanges();
        }



    }
}